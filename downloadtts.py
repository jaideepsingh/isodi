import config, urllib
from utils import getdata
from lxml import etree

def getdownload(conversionnumber):
    finalurl = config.getttsurl % ("GetConversionStatus", config.email, config.accountid, conversionnumber)
    #print finalurl
    the_page = getdata(finalurl)
    root = etree.XML(the_page)
    downloadurl = root.get('downloadUrl')
    filename = "result_"+str(conversionnumber)+".wav"
    urllib.urlretrieve(downloadurl, filename) #download the tts file
    print "saved tts file to "+filename
    
def main():
    #For testing
    getdownload(25)
    
if __name__ == "__main__":
    main()