import urllib, urllib2, json, re, sys
from utils import getdata, urlfix, replace_all, trimTrackInfo
from fetchinfofromwiki import fetchTrackInfoWiki

def fixString(inputstring):
    chars = {'&quot;':'','&amp;':'&','-CM':'','</em>':'','</span>':''}
    inputstring = replace_all(inputstring,chars)
    inputstring = inputstring[:-132]
    inputstring = re.sub(r'\<.*?\>','',inputstring)
    return inputstring

def fixInfo(inputstring):
    return urllib.quote(inputstring)

def fetchTrackInfo(artist, track):
    #print "artist: "+artist+"track: "+track
    apikey = "79d0e4365d873b124d0708f0abdae414"
    url = "http://ws.audioscrobbler.com/2.0/?method=track.getInfo&api_key=%s&artist=%s&track=%s&format=json"
    #print url % (apikey, urlfix(artist), urlfix(track))
    jsonstring = getdata(url % (apikey, fixInfo(artist), fixInfo(track)))
    jsonstring = json.loads(jsonstring)
    if jsonstring['track']['wiki']['content'] and len(trimTrackInfo(fixString(jsonstring['track']['wiki']['content']))) > 50:
        return trimTrackInfo(fixString(jsonstring['track']['wiki']['content']))
    else:
        print "cannot find info on last.fm trying wikipedia"
        returnstring = fetchTrackInfoWiki(artist, track)
        return trimTrackInfo(returnstring)

def main():
    #For testing
    artist = "Coldplay"
    song = "Don't Let It Break Your Heart"
    hello =  trimTrackInfo(fetchTrackInfoWiki(artist, song))
    print hello

if __name__ == "__main__":
    main()