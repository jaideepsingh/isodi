import urllib, urllib2,re
from bs4 import BeautifulSoup
from utils import trimTrackInfo

def fetchTrackInfoWiki(artist,track):
    return getarticle(urllib.quote(track))

def getarticle(article):
    opener = urllib2.build_opener()
    opener.addheaders = [('User-agent', 'Mozilla/5.0')]
    resource = opener.open("http://en.wikipedia.org/wiki/" + article)
    data = resource.read()
    resource.close()
    
    soup = BeautifulSoup(data)
    text = soup.find('div',id="bodyContent")
    paras = text.find_all('p', limit=2) # get first two paras
    outputstring = ''
    for para in paras:
        para = re.sub(r'\<.*?\>','',str(para)) #remove html tags
        para = re.sub(r'\[.*?]','',str(para)) #remove []s
        outputstring += para
    return trimTrackInfo(outputstring) #get ~100 words

def main():
    #for testing
    track = "Lost!"
    fetchTrackInfoWiki('',track)
    
if __name__ == "__main__":
    main()