def generatePlaylist(mp3list):
    #mp3list is in format
    playlist = open("playlist.m3u",'a')
    for songname, value in mp3list.iteritems():
        playlist.write("#EXTINF:"+str(value['length'])+",")
        playlist.write(value['artist']+"-"+value['title'])
        playlist.write("\n")
        playlist.write(songname)
        playlist.write("\n")
    
    playlist.close()
    #if exits open add at end 
    
    #else create and add
    
def main():
    #for testing
    dict1 = {'song1.mp3' : {'length' : 123, 'artist' : 'artist1', 'title' : 'title1'}}
    dict2 = {'song1.mp3' : {'length' : 123, 'artist' : 'artist1', 'title' : 'title1'}, 'song2.mp3' : {'length' : 213, 'artist' : 'artist2', 'title' : 'title2'}}
    generatePlaylist(dict2)
    
if __name__ == "__main__":
    main()