ISODI
=========
Internet Streaming Operated by Dj Inrealtime

isodi radio is an automated dj radio with a awesome capabilities:

  - Can fetch track and artist info
  - Can fetch local music events
  - Can fetch local weather
  - Can fetch local news
  - Can play recordings of famous audios (much later)

Version Plan
------
  - 0.0.1 : generate a playlist to with dj saying 3 lines about the songs
    -  get info from wiki if last.fm doesnt have it
    - randomize tts speaker
  - 0.0.2 : remove "brought to you by neospeech" and check if getting info is working well for atleast 50 songs
    -  specify a process (look at song name, if not found, look at artist name etc)
  - 0.0.3 : add "This is isodi radio.. enjoy the time is .." at the end of song
    - dont sendtts if later steps will crash
  - 0.0.4 : Release to GAE
    - Can first just do time on GAE, will add song details later on..
  - 0.0.5 : get latest events about the artist
  - 0.0.6 : make it take multiple free neospeech accounts
  - 0.0.7 : find a way to query for ID3 tags
  - 0.0.8 : integrate with web player
  - 0.0.9 : make web mockup of isodi.com
  - 0.1   : make it shoutcast compatible

Todo
------

Dillinger uses a number of open source projects to work properly:

1.   fix the fucking url issues once in for all
2.   if can't find on last.fm search the fucking wikipedia
3.   use requests instead of urllib2. It apparently sucks for sending tts

API 
-
Closed for now

License
-
Closed for now