from utils import getdata, urlfix
from fetchinfo import fetchTrackInfo
import config
from lxml import etree #need this
from random import choice

#16bit linear PCM Wave format

def ttsmagic(text):
    ttsvoices = ['TTS_PAUL_DB','TTS_KATE_DB','TTS_JULIE_DB']
    voice = choice(ttsvoices)
    finalurl = config.sendttsurl % ("ConvertSimple", config.email, config.accountid, config.loginkey, config.password, voice, config.outputformat, config.samplerate, urlfix(text))
    print finalurl
    the_page = getdata(finalurl)
    root = etree.XML(the_page)
    return root.get('conversionNumber')
    
def main():
    #for testing
    text = fetchTrackInfo("coldplay","yellow")
    the_page = ttsmagic(text)
    print the_page

if __name__ == "__main__":
    main()