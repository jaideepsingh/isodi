from pydub import AudioSegment
import sys
'''
pip install pydub
needs ffmpeg
todo: figure out why/how to supress assert warnings
'''

def mixvoicentrack(voice,track,filename):
    talk = AudioSegment.from_wav(voice) #get talk
    song = AudioSegment.from_mp3(track) #get song

    begin_song = song[:(len(talk)+1000)] #cut first len seconds of song
    begin_song = begin_song - 8 #lower volume of len seconds of song by 10db
    beginning = begin_song.overlay(talk) #overlay talk with len song

    rest_song = song[(len(talk)+1000):len(song)] #get rest of song
    rest_song_beginning = rest_song[:1000].fade(0,-8,0,1000) #add easy fade first second in
    rest_song_end = rest_song[1000:len(rest_song)] #rest of rest of song

    out = beginning + rest_song_beginning + rest_song_end # add em up
    outlen = len(out)/1000
    out.export(filename, format="mp3") # save the output (took ~6 sec)
    print "done."
    return outlen

def main():
    #for testing
    mixvoicentrack("result_28.wav","files/zedd-clarity-tom-budin-remix.mp3","outputzedd.mp3") #these should exist
    
if __name__=="__main__":
    main()