import sys,time
import songdetails #from https://github.com/Ciantic/songdetails
from fetchinfo import fetchTrackInfo
from fetchinfofromwiki import fetchTrackInfoWiki
from sendtracktts import ttsmagic
from downloadtts import getdownload
from songprocess import mixvoicentrack
from generateplaylist import generatePlaylist

def main():
    #this is the main main
    print "So you want to convert "+str(sys.argv[1])+" to "+str(sys.argv[2])+"?"
    songfile = str(sys.argv[1])
    processedfile = str(sys.argv[2])
    song = songdetails.scan(songfile)
    print "song details: Artist: "+song.artist+" Title: "+song.title
    returnedInfoText = fetchTrackInfo(song.artist, song.title)
    print "This is the text to be ttsed:\n"+returnedInfoText
    answer = raw_input("is this ok? (ya/no):")
    if answer == 'ya':
        conversionnumber = ttsmagic(returnedInfoText)
        print "Wait 5 sec.."
        time.sleep(5)
        getdownload(conversionnumber)
        ttsAudioFileLoc = "result_"+str(conversionnumber)+".wav"
        print "Downloaded tts file! Converting.. (This takes time)"
        outlen = mixvoicentrack(ttsAudioFileLoc,songfile,processedfile)
        print "final file processed and saved!"
        print "Adding to playlist.."
        songdict = {processedfile : {'length' : outlen, 'artist' : song.artist, 'title' : song.title}}
        generatePlaylist(songdict)
        print 'done'
        
    else:
        print "ok lets try wikipedia"
        returnedInfoText = fetchTrackInfoWiki(song.artist, song.title)
        print "This is the text to be ttsed:\n"+returnedInfoText
        answer = raw_input("Now its ok? (ya/no):")
        if answer == 'ya':
            conversionnumber = ttsmagic(returnedInfoText)
            print "Wait 5 sec.."
            time.sleep(5)
            getdownload(conversionnumber)
            ttsAudioFileLoc = "result_"+str(conversionnumber)+".wav"
            print "Downloaded tts file! Converting.. (This takes time)"
            outlen = mixvoicentrack(ttsAudioFileLoc,songfile,processedfile)
            print "final file processed and saved!"
            print "Adding to playlist.."
            songdict = {processedfile : {'length' : outlen, 'artist' : song.artist, 'title' : song.title}}
            generatePlaylist(songdict)
            print 'done'
        else:
            print "Exiting then.."
        
if __name__=="__main__":
    main()