import urllib,urllib2,re

def replace_all(text, dic):
    for i, j in dic.iteritems():
        text = text.replace(i, j)
    return text

def getdata(url):
    req = urllib2.Request(url)
    response = urllib2.urlopen(req)
    the_page = response.read()
    return the_page

def urlfix(url):
    '''removes all special characters and adds the + for url safeness'''
    #urllib.quote_plus(url, safe="%/:=&?~#+!$,;'@()*[]") #find how this works
    #print url.replace(" ","+")
    return url.replace(" ","+")

def trimTrackInfo(inputstring):
    outputstring =''
    words = inputstring.split()
    for word in words[:98]: #need this for 100 word limit on neospeech
        outputstring += word
        outputstring += " "
    outputstring = remove_last(outputstring) #print till last fullstop
    return outputstring

def remove_last(source):
    lastdot = source.rfind(".")
    new = source[:lastdot+1] 
    return new